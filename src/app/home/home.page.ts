import { Component, OnInit } from '@angular/core';
import { DataService, Message } from '../services/data.service';
import { PocketapiService } from '../services/pocketapi.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit{

  pockemons: any[]= [];

  constructor(private data: DataService, private pockmonService: PocketapiService) {}


  ngOnInit(): void {
    this.pockmonService.getAllPockemons()
    .subscribe((res: any) => {
      console.log(res);
      this.pockemons = res?.results;
      console.log('this.pockemons', this.pockemons);
    });

    console.log('Method not implemented.');
  }

  refresh(ev) {
    setTimeout(() => {
      ev.detail.complete();
    }, 3000);
  }

  getMessages(): Message[] {
    return this.data.getMessages();
  }

  trackByFnPockemons(_index: number, message: any): string {
    return message.name;
  }

}
