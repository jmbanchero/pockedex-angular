import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class PocketapiService {

  constructor(private http: HttpClient) {

   }

   getAllPockemons() {
    console.log('service');
    // return this.http.get('http://localhost:3001/pokemons');
    return this.http.get('https://workshop-nestjs-pokeapi.vercel.app/pokemons')
   }
}
