import { TestBed } from '@angular/core/testing';

import { PocketapiService } from './pocketapi.service';

describe('PocketapiService', () => {
  let service: PocketapiService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PocketapiService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
