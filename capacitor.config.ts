import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'io.ionic.starter',
  appName: 'pockedex-angular',
  webDir: 'www',
  bundledWebRuntime: false
};

export default config;
